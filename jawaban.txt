Soal 1 Membuat Database
CREATE DATABASE myshop;

Soal 2 Membuat Table di Dalam Database

users
CREATE TABLE users( id int(8) PRIMARY KEY AUTO_INCREMENT, name varchar(255) NOT null, email varchar(255) NOT null, password varchar(255) NOT null );

categories
CREATE TABLE categories( id int(8) AUTO_INCREMENT PRIMARY KEY, name varchar(255) NOT null );

items
CREATE TABLE items( id int(8) AUTO_INCREMENT PRIMARY KEY, name varchar(255) NOT null, description varchar(255) NOT null, price int(10) NOT null, stock int(10) NOT null, categories_id int(8) NOT null, FOREIGN KEY(categories_id) REFERENCES categories(id) );


Soal 3 Memasukkan Data pada Table

users
INSERT into users(name, email, password) VALUES ("John Doe", "john@doe.com", "john123"), ("Jane Doe", "jane@doe.com","jenita123");

categories
INSERT into categories(name) VALUES("gadget"),("cloth"),("men"),("women"),("branded");

items
INSERT INTO items(name,description,price,stock,categories_id) VALUES("Sumsang b50", "hape keren dari merek sumsang",4000000, 100, 1), ("Uniklooh","baju keren dari brand ternama", 500000,50,2), ("IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);

Soal 4 Mengambil Data dari Database

a. Mengambil data users
SELECT id,name,email from users;

b. Mengambil data items
SELECT * FROM items WHERE price > 1000000;

SELECT * FROM items WHERE name LIKE "uniklo%";

c. Menampilkan data items join dengan kategori
SELECT items. *,categories.name as kategori FROM items INNER JOIN categories ON items.categories_id = categories.id;

Soal 5 Mengubah Data dari Database

UPDATE items set price=2500000 WHERE id=1;





