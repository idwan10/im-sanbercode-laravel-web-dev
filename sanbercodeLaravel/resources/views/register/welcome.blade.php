@extends('layout.extend')
@section('title')
Halaman welcome
@endsection
@section('sub-title')
welcome
@endsection
@section('content')
    <h1>SELAMAT DATANG {{$namaDepan}} {{$namaBelakang}} !</h1>
    <h2>Terima kasih telah bergabung di Sanberbook. Social Media kita bersama!</h2>
@endsection