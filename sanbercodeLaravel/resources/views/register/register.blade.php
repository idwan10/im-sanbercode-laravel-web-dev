@extends('layout.extend')
@section('title')
Halaman Buat Akun
@endsection
@section('content')
    <h2>Sign Up Form</h2>
<form action="/welcome" method="post">
@csrf
    <label>First name</label><br>
    <input type="text" name="fname"><br><br>
    <label>Last name</label><br>
    <input type="text" name="lname"><br><br>
    <label>Gender</label><br>
    <input type="radio">Male<br>
    <input type="radio">Female<br>
    <input type="radio">Other<br><br>
    <label>Nationality:</label> <br>
    <select name="country">
        <option value="Indonesia">Indonesia</option>
        <option value="American">American</option>
        <option value="Arabic">Arabic</option>
        <option value="Malaysia">Malaysia</option>
    </select><br><br>
    <label>Language Spoken</label><br>
    <input type="checkbox" name="Language">English<br>
    <input type="checkbox" name="Language">Indonesia<br>
    <input type="checkbox" name="Language">Arabic<br>
    <input type="checkbox" name="Language">Other<br><br>
    <label>Bio</label><br>
    <textarea name="message" row="10" cols="5"></textarea>
    <br><br>
    <input type="submit" value="Submit">
</form>
@endsection