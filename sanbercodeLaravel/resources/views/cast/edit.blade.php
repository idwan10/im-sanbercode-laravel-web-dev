@extends('layout.extend')
@section('title')
Halaman Edit Cast
@endsection
@section('sub-title')
Cast
@endsection
@section('content')

<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Cast Nama</label>
      <input type="text" name="nama" value="{{$cast->nama}}" class="form-control">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Cast Umur</label>
      <input type="text" name="umur" value="{{$cast->umur}}" class="form-control">
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
      <label>Cast Bio</label>
      <textarea name="Bio" class="form-control" cols="30" rows="10">{{$cast->bio}}></textarea>
    </div>
    @error('Bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection