<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function Register()
    {
        return view('register.register');
    }

    public function welcome(Request $request)
    {
        //dd($request->all());
        $namaDepan= $request['fname'];
        $namaBelakang= $request['lname'];

        return view('register.welcome', ['namaDepan'=> $namaDepan, 'namaBelakang'=> $namaBelakang]);
    }
}
