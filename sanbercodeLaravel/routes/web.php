<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[HomeController::class,'home'] );

Route::get('/register',[AuthController::class,'register'] );

Route::post('/welcome',[AuthController::class,'welcome'] );

Route::get('/table', function(){
    return view('register.table');
});

Route::get('/data-tables', function(){
    return view('register.data-tables');
});

//CRUD

//Create Data
//Route mengarah ke form tambah cast
Route::get('/cast/create',[CastController::class,'create']);
//Route untuk menyimpan inputan kedalam database
Route::post('/cast',[CastController::class,'store']);

#Read Data
//Route mengarah ke halaman tampilan data di tabel cast
Route::get('/cast', [CastController::class,'index']);
//Route Detail Cast berdasarkan ID
Route::get('/cast/{cast_id}', [CastController::class,'show']);

#Updaet Data
//Route mengarah form edit cast

Route::get('/cast/{cast_id}/edit',[CastController::class,'edit']);

//Route untuk edit data berdasarkan ID
Route::put('/cast/{cast_id}', [CastController::class,'updaet']);

#Delete Data
Route::delete('/cast/{cast_id}', [CastController::class,'destroy']);